/**
 ** @author:	 Greedysky
 ** @date:       2019.4.17
 ** @brief:      进度条组件
 */
#ifndef RINGSPROGRESSBAR_H
#define RINGSPROGRESSBAR_H

#include <QWidget>

class RingsProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit RingsProgressBar(QWidget *parent = nullptr);

    void setValue(int value);

protected:
    virtual void paintEvent(QPaintEvent *event) override;

private:
    int m_angle, m_value;

};

#endif

/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      按钮组件
 */
#ifndef RADIOBUTTON_H
#define RADIOBUTTON_H

#include "checkable.h"

class RadioButton : public Checkable
{
    Q_OBJECT
public:
    explicit RadioButton(QWidget *parent = nullptr);

protected:
    virtual void setupProperties() override;

};

#endif
